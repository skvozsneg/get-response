import json
from typing import Union, Generator

from .api_wrapper import ApiWrapper


class RestWrapper(ApiWrapper):
    def parse_response(self) -> None:
        obj_for_parse = json.loads(self.cur_obj)
        for field_name, to_find_road in self.to_find.items():
            for parse_result in self._parse(obj_for_parse, to_find_road):
                self.founded.update({field_name: parse_result})

    @classmethod
    def _parse(cls,
               obj_for_parse: dict,
               to_find_road: list) -> Generator[Union[str, dict, list, None], None, None]:
        for to_find_field in to_find_road:
            yield cls._parse_one(obj_for_parse, to_find_field)

    @classmethod
    def _parse_one(cls, obj_for_parse: dict, to_find_field: str) -> Union[str, dict, list, None]:
        item: Union[str, dict, list, None] = None
        try:
            return obj_for_parse[to_find_field]
        except KeyError:
            for value in obj_for_parse.values():
                if isinstance(value, dict):
                    item = cls._parse_one(value, to_find_field)
                if isinstance(value, list):
                    for list_value in value:
                        if isinstance(list_value, dict):
                            item = cls._parse_one(list_value, to_find_field)
        return item
